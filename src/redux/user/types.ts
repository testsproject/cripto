export interface IUser {
    email: string,
    password: string,
}

export interface IAuthState {
    users: IUser[],
    currentUser: IUser,
    unknownUser: boolean,
    isAuth: boolean,
}