import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IAuthState, IUser } from './types';

const initialState: IAuthState = {
    users: [{email: "admin@admin.ru", password: "admin123"}],
    currentUser: {email: "", password: ""},
    unknownUser: false,
    isAuth: false
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state: IAuthState, action: PayloadAction<IUser>) => {
        state.users.push(action.payload);
        state.currentUser = action.payload;
        state.isAuth = true;
    },
    loginUser: (state: IAuthState, action: PayloadAction<IUser>) => {
      state.users.forEach(user => {
        if (JSON.stringify(user) === JSON.stringify(action.payload)) {
          state.currentUser = action.payload;
          state.isAuth = true;
        } else {
          state.unknownUser = true;
        }
      })
    },
    logout: (state: IAuthState) => {  
        state.unknownUser = false;      
        state.isAuth = false;
    },
    cancel: (state: IAuthState) => {  
        state.unknownUser = false;
    },

  },
});

export const { setUser, logout, loginUser, cancel } =
  userSlice.actions;

export default userSlice.reducer;
