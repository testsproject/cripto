import { configureStore } from '@reduxjs/toolkit';
import user from './user/slice';
import cripto from './cripto/slice';

export const store = configureStore({
  reducer: {
    user,
    cripto
  },
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
