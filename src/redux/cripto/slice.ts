import { createSlice, createEntityAdapter, createAsyncThunk } from '@reduxjs/toolkit';
import axios from "axios";
import { ICoin, IParams } from './types';
import {RootState} from '../store'

export const fetchCriptoData = createAsyncThunk('cripto/fetchDataCripto', async (params: IParams, { dispatch }) => {
    const {page, per_page} = params;
    try {
        const {data} = await axios.get<ICoin[]>(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=${per_page}&page=${page}`);
        return data;
    } catch(e) {
        return(e.message);
    }    
})

const coinsAdapter = createEntityAdapter<ICoin>({
  selectId: (coin: ICoin) => coin.id,
})

const criptoSlice = createSlice({
  name: 'cripto',
  initialState: coinsAdapter.getInitialState({loading: false, error: '', params: {page: 1, per_page: 35}}),
  reducers: {
    fetchMore: (state) => {
        state.params = {page: state.params.page + 1, per_page: state.params.per_page};
    },
  },
  extraReducers: {
    [fetchCriptoData.pending.toString()](state) {
      state.loading = true;
    },
    [fetchCriptoData.fulfilled.toString()](state, {payload}) {
      state.loading = false;
      coinsAdapter.setAll(state, payload)
    },
    [fetchCriptoData.rejected.toString()](state, {payload}) {
      state.loading = false;
      state.error = payload;
    }
  }
});

export const { fetchMore } = criptoSlice.actions;

export const coinSelectors = coinsAdapter.getSelectors<RootState>(state => state.cripto)

export default criptoSlice.reducer;
