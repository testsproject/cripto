import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { ICoin, IParams } from "./types";

export const fetchCripto = {
    fetchCriptoData: createAsyncThunk<ICoin[], IParams>('cripto/fetchDataCripto', async (params, { rejectWithValue }) => {
    const {page, per_page} = params;
    try {
        const {data} = await axios.get<ICoin[]>(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=${per_page}&page=${page}`);
        return data;
    } catch(e) {
        return rejectWithValue(e.message);
    }    
})
}

