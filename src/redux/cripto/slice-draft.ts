import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchCripto } from './fetchCriptoData';
import { IParams, Status, ICriptoSliceState } from './types';

const initialState: ICriptoSliceState = {
  items: [],
  status: Status.LOADING,
  isError: '',
  params: {
    page: 1,
    per_page: 35,
  }  
};

const criptoSlice = createSlice({
  name: 'cripto',
  initialState,
  reducers: {
    fetchMore: (state: ICriptoSliceState) => {
        state.params = {page: state.params.page + 1, per_page: state.params.per_page};
    },
  },

    extraReducers: (builder) => {
        builder.addCase(fetchCripto.fetchCriptoData.pending, (state) => {
            state.status = Status.LOADING;
            state.items = [];
        });
        builder.addCase(fetchCripto.fetchCriptoData.fulfilled, (state, action) => {
            state.items = action.payload;
            state.status = Status.SUCCESS;
        });
        builder.addCase(fetchCripto.fetchCriptoData.rejected, (state, action: PayloadAction<any>) => {
            state.status = Status.ERROR;
            state.items = [];
            state.isError = action.payload;
        });
    }
});

export const { fetchMore } = criptoSlice.actions;

export default criptoSlice.reducer;
