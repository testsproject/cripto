import { FC } from "react";
import { useActions, useTypedSelector } from "./hooks";
import "./scss/app.scss";
import Auth from "./components/auth/Auth";
import AppRouter from "./router/AppRouter";

const App: FC = () => <AppRouter/>

export default App;
