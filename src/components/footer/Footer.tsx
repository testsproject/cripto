import { FC } from 'react';
import './Footer.scss';

import vk from '../../assets/img/vk.svg';
import tg from '../../assets/img/tg.svg';
import ok from '../../assets/img/ok.svg';


const Footer: FC = () => {
    return (
        <div className="footer">
            <a href="https://vk.com/">
                <img src={vk} alt="vk"/>
            </a>
            <a href="https://web.telegram.org">
                <img src={tg}/>
            </a>
            <a href="https://ok.ru/">
                <img src={ok}/>
            </a>
        </div>
    );
};

export default Footer;