import { FC } from 'react';
import './Input.scss'

interface IInputProps {
    key: React.Key;
    type?: string;
    label: string;
    value: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    errorMesage?: string,
    valid: boolean,
    touched: boolean,
    shouldValidate: boolean,
}

function isInvalid({valid, touched, shouldValidate}) {
    return !valid && shouldValidate && touched
}

const Input: FC<IInputProps> = (props) => {
    const {type = "text", label, value, onChange, errorMesage} = props;
    const classes = (isInvalid(props) ? "inputBlock invalid" : "inputBlock" )
    const htmlF = `${type}-${Math.random()}`

    return (
        <div className={classes}>
            <label htmlFor={htmlF}>{label}</label>
            <input id={htmlF} type={type} value={value} onChange={(e) => onChange(e)} />
            <span>{errorMesage}</span>          
        </div>
    );
};

export default Input;