import { FC } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useActions, useTypedSelector } from '../../hooks';
import Button from '../button/Buttton';
import './Header.scss'

const Header: FC = () => {
    const { logout } = useActions();
    const { currentUser } = useTypedSelector((state) => state.user);
    const location = useLocation();
    const navigate = useNavigate();

    const clickLogout = () => {
        logout();
        navigate('/');
    }

    return (
        <div className='header'>
            <Button onClick={clickLogout}>Log out</Button>         
            <Link to={location.pathname === '/' ? '/profile' : '/'}>
                <Button>
                    {location.pathname === '/' ? 'Profile page' : 'Main page'}
                </Button>
            </Link>
            
            <div className="header-profile">
                {currentUser.email}
            </div>
            
        </div>
    );
};

export default Header;