import {FC, ButtonHTMLAttributes } from 'react';
import styles from './Button.module.scss';

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    onClick?: (e: React.MouseEvent) => void,
    children: React.ReactNode,
    disabled?: boolean;
}

const Button: FC<IButtonProps> = ({ onClick, children, disabled}) => {
    return (
        <button onClick={onClick} className={styles.btn} disabled={disabled}>
            {children}
        </button>
    );
};

export default Button;