import { FC, useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { useActions, useTypedSelector } from '../../hooks';
import Button from '../button/Buttton';
import Input from '../input/Input';

import styles from './Auth.module.scss'

interface IValidation {
    required: boolean, 
    minLength?: number, 
    email?: boolean, 
    latin?: boolean
}

interface IStateItem {
    value: string,
    type: string,
    label: string,
    errorMesage: string,
    valid: boolean,
    touched: boolean,
    validation: IValidation
}

type IState = Record<string, IStateItem>

const validateEmail = (email: string) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

const validatePasswordLatin = (password: string) => {
    return String(password)
      .toLowerCase()
      .match(
        /^[a-z0-9]+$/i
        );
};

const Auth: FC = () => {
    const { loginUser, setUser, cancel} = useActions();
    const { unknownUser  } = useTypedSelector((state) => state.user);

    const [formControls, setFormControls] = useState<IState>({
        email: {
            value: "",
            type: "email",
            label: "e-mail",
            errorMesage: "Please enter a valid e-mail",
            valid: false,
            touched: false,
            validation: {
                required: true,
                email: true
            }
        },
        password: {
            value: "",
            type: "password",
            label: "password",
            errorMesage: "Only numbers and Latin, at least 8 characters",
            valid: false,
            touched: false,
            validation: {
                required: true,
                minLength: 8,
                latin: true
            }
        },
        confirmPassword: {
            value: "",
            type: "password",
            label: "confirm password",
            errorMesage: "Only numbers and Latin, at least 8 characters",
            valid: false,
            touched: false,
            validation: {
                required: true,
                minLength: 8,
                latin: true
            }
        }
       
    })

    const navigate = useNavigate();
    const location = useLocation();

    const signInHandler = () => {
        const objForm = {email: formControls.email.value, password: formControls.password.value }
        loginUser(objForm);
    }

    const submitHandler = (e: React.FormEvent) => {
        e.preventDefault();
        
    }

    const signUpHandler = () => {
        const objForm = {email: formControls.email.value, password: formControls.password.value }
        setUser(objForm);
    }

    const validateControl = (value: string, validation: IValidation, type: string) => {
        if (!validation) {
            return true;
        }
        let isValid = true;

        if (validation.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (validation.email)  {
            isValid = validateEmail(value) && isValid;
        }

        if (validation.minLength) {
            isValid = value.length >= validation.minLength && isValid;
        }
        
        if (validation.latin) {
            isValid = validatePasswordLatin(value) && isValid;
        }

        return isValid;
    }

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>, controlName: string, ) => {
        const formControlsState = {...formControls}
        const control = {...formControlsState[controlName]}
        control.value = e.target.value;
        control.touched = true;
        control.valid = validateControl(control.value, control.validation, control.type);
        formControlsState[controlName] = control;
        setFormControls(formControlsState)
    }
    
    const deleteKeyConfirmFromState = (obj: IState) => {
        const copyStateObject = {...obj};
        if (location.pathname !== "/register") {
            delete copyStateObject.confirmPassword;
        }
        return copyStateObject;
    }

    const renderInputs = () => {
        const convertStateObject = deleteKeyConfirmFromState(formControls);
       return Object.keys(convertStateObject).map((controlName, index) => {
        const control = formControls[controlName];
            return (
                <Input
                    key={controlName + index}
                    type={control.type}
                    value={control.value}
                    valid={control.valid}
                    touched={control.touched}
                    label={control.label}
                    shouldValidate={!!control.validation}
                    errorMesage={control.errorMesage}
                    onChange={(e) => {onChangeHandler(e, controlName)}}
                />
            )
        })
    }

    const disabledButton = () => {  
        let flag = false;      
        Object.keys(deleteKeyConfirmFromState(formControls)).forEach(item => {            
            if (!formControls[item].valid) {
                flag = true;
            }
        })
        return flag;
    }

    const confirmPassword = () => {      
        if (!formControls.password.value) {
            return true;
        }
        if (formControls.password.value === formControls.confirmPassword.value) {
            return false;
        }
        return true;
    }

    const cancelHendler = () => {
        cancel();
        navigate("/");
    }

    return (
        <form onSubmit={submitHandler} className={styles.authForm}>
            <h2>{location.pathname === "/" ? "Authorization" : "Registration"}</h2>
            {unknownUser && location.pathname === "/" ? <p>Unknown user, you need to register!</p> : null}
            {renderInputs()}
            <div className={styles.buttonBlock}>
                {location.pathname === "/" ? (
                    <>
                        <Button onClick={signInHandler} disabled={disabledButton()}>Sing In</Button>
                        <Button onClick={() => navigate('/register')}>Sing Up</Button>
                    </>
                    
                ) : (
                    <>
                        <Button onClick={signUpHandler} disabled={disabledButton() && confirmPassword()}>Submit</Button>
                        <Button onClick={cancelHendler}>Cancel</Button>
                    </>
                )}
                
            </div>               
        </form>
    );
};

export default Auth;
