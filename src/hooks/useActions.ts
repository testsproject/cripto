import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AppDispatch } from '../redux/store';
import { fetchCripto } from '../redux/cripto/fetchCriptoData';
import { setUser, logout, loginUser, cancel } from '../redux/user/slice';
import { fetchMore } from '../redux/cripto/slice';

const allActionCreators = {
  ...fetchCripto,
  setUser,
  logout,
  loginUser,
  cancel,
  fetchMore,
};

export const useActions = () => {
  const dispatch = useDispatch<AppDispatch>();
  return bindActionCreators(allActionCreators, dispatch);
};
