
import { FC } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { useTypedSelector } from '../hooks';
import AuthLayout from '../layouts/authLayout/authLayout';
import MainLayout from '../layouts/mainLayout/mainLayout';
import LoginPage from '../pages/loginPage/LoginPage';
import MainPage from '../pages/mainPage/MainPage';
import NotFoundPage from '../pages/notFoundPage';
import ProfilePage from '../pages/profilePage/ProfilePage';
import RegisterPage from '../pages/registerPage/RegisterPage';

const AppRouter: FC = () => {
    const { isAuth } = useTypedSelector((state) => state.user);

    return isAuth ? (
        <Routes>
            <Route path="/" element={<MainLayout />}>
                <Route index element={<MainPage />} />
                <Route path="profile" element={<ProfilePage />} />
                <Route path="register" element={<Navigate to={'/'}/>} />
                <Route path="*" element={<NotFoundPage />}/>
            </Route>                
        </Routes>
    ) : (
        <Routes>
            <Route path="/" element={<AuthLayout />}>
                <Route index element={<LoginPage />} />
                <Route path='register' element={<RegisterPage />} />
                <Route path="*" element={<Navigate to="/login" />} />                
            </Route>            
        </Routes>
    );
};

export default AppRouter;