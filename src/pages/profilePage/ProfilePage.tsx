import { FC } from 'react';
import { useTypedSelector } from '../../hooks';

import './ProfilePage.scss'

const ProfilePage: FC = () => {
    const { currentUser } = useTypedSelector((state) => state.user);
    
    return (
        <div className="profilePage">
            <h2>Profile page</h2>
            <div>{currentUser.email}</div>
            <div>{currentUser.password}</div>
        </div>
    );
};

export default ProfilePage;