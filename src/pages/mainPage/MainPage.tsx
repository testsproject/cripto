import { FC, useEffect, useState } from 'react';
import Button from '../../components/button/Buttton';
import { useActions, useTypedSelector } from '../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCriptoData, coinSelectors } from '../../redux/cripto/slice';

import './MainPage.scss';
import { AppDispatch } from '../../redux/store';

const MainPage: FC = () => {
    const { fetchMore } = useActions();
    const { params } = useTypedSelector((state) => state.cripto);

    const { loading, error } = useTypedSelector((state) => state.cripto);
    const allCoins = useSelector(coinSelectors.selectAll);
    const dispatch = useDispatch<AppDispatch>();

    useEffect(() => {
        dispatch(fetchCriptoData(params));
    }, [params])

    const moreItems = () => {
        fetchMore();
        fetchCriptoData(params);
    }

    return (
        <div className="mainPage">
            <h2>Main Page</h2>
            <Button onClick={moreItems} disabled={loading}>Fetch more</Button>
            <div className="mainPage-list">
                {loading ? "Loading..." : allCoins.map((coin, index) => {
                    return (
                        <div key={coin.id}>{index + 1}.  {coin.name}  {coin.current_price}$</div>
                    )
                })}
            </div>           
        </div>
    );
};

export default MainPage;

