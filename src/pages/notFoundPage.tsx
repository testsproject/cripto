import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../components/button/Buttton';

const NotFounPage: FC = () => {
    const navigate = useNavigate();

    return (
        <div>
            <h2>Страница не найдена</h2>
            <Button onClick={() => navigate('/')}>На главную</Button>
        </div>
    );
};

export default NotFounPage;