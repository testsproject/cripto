import { FC } from 'react';
import { Outlet } from 'react-router-dom';

import './authLayout.scss'


const AuthLayout: FC = () => {
    return (
        <div className='authLayout'>
            <Outlet />
        </div>
    );
};

export default AuthLayout;